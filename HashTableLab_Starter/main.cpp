#include <iostream>
#include <string>
#include <fstream>
using namespace std;

#include "DATASTRUCTURES/DictionaryTester.hpp"
#include "SchoolProgram.hpp"
#include "cuTEST/Menu.hpp"
#include "UTILITIES/Logger.hpp"

int main()
{
    Logger::Setup( false );
    while ( true )
    {
        Menu::Header( "MAIN MENU" );
        int choice = Menu::ShowIntMenuWithPrompt( { "Tests", "Program", "Exit" } );

        if ( choice == 1 )
        {
            DictionaryTester test;
            test.Start();
        }
        else if ( choice == 2 )
        {
            SchoolProgram program;
            program.Start();
        }
        else if ( choice == 3 )
        {
            break;
        }

        cout << endl;
    }
    Logger::Cleanup();

    return 0;
}
