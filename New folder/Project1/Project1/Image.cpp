#include "Image.hpp"

#include <fstream>
#include <iostream>
#include <exception>
using namespace std;

void PpmImage::LoadImage(const string& filename)
{
    cout << "Loading image from \"" << filename << "\"...";
    ifstream input;
    input.open(filename);
    if (input.fail())
    {
        cout << "Input File opening failed" << endl;
        exit(1);
    }

    int intBuffer;
    string strBuffer;


    getline(input, strBuffer); // Skip "P3"
    getline(input, strBuffer); // Skip comment

    input >> intBuffer; // Skip width
    input >> intBuffer; // Skip height

    input >> m_colorDepth;

    int x = 0;
    int y = 0;

    while (input >> m_pixels[x][y].r
        >> m_pixels[x][y].g
        >> m_pixels[x][y].b)
    {
        x++;
        if (x == IMAGE_WIDTH)
        {
            y++;
            x = 0;
        }
    }

    input.close();
    
   
    
        
    
    
    //throw runtime_error("Method not implemented!");

    cout << "SUCCESS" << endl;
}

void PpmImage::SaveImage(const string& filename)
{
    cout << "Saving image to \"" << filename << "\"...";

    ofstream output;
    output.open(filename);
    if (output.fail())
    {
        cout << "Output File opening failed " << endl;
        exit(1);
    }
    output << "P3\n";
    output << "# Comment\n";
     
    output << IMAGE_WIDTH << " " << IMAGE_HEIGHT << endl;
    output << m_colorDepth << endl;

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
    for (int x = 0; x < IMAGE_WIDTH; x++)
        {
        output
            << m_pixels[x][y].r << endl
            << m_pixels[x][y].g << endl
            << m_pixels[x][y].b << endl;
        }
    }

    
    output.close();
    //throw runtime_error("Method not implemented!");

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter1()
{
    cout << "Applying filter 1...";

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
    for (int x = 0; x < IMAGE_WIDTH; x++)
        {
        
        int r = m_pixels[x][y].r - 120;
        int g = m_pixels[x][y].g - 120;
        int b = m_pixels[x][y].b - 120;
        if (r < 0)
        {
            r = 0;
        }
        if (b < 0)
        {
            b = 0;
        }
        if (g < 0)
        {
            g = 0;
        }
            
        m_pixels[x][y].r = r;
        m_pixels[x][y].g = g;
        m_pixels[x][y].b = b;
        }
    }

    //throw runtime_error("Method not implemented!");

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter2()
{
    cout << "Applying filter 2...";

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            

            int r = m_pixels[x][y].r + 120;
            int g = m_pixels[x][y].g + 120;
            int b = m_pixels[x][y].b + 120;
            if (r > 255)
            {
                r = 255;
            }
            if (b > 255)
            {
                b = 255;
            }
            if (g > 255)
            {
                g = 255;
            }

            m_pixels[x][y].r = r;
            m_pixels[x][y].g = g;
            m_pixels[x][y].b = b;
        }
    }
    //throw runtime_error("Method not implemented!");

    cout << "SUCCESS" << endl;
}
