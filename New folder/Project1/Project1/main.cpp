#include "Image.hpp"

#include <iostream>
#include <string>
using namespace std;

int main()
{
    bool done = false;
    string filename;
    int filter;

    while (!done)
    {
        cout << "---------------------" << endl;
        cout << "Work with image: ";
        getline(cin, filename);

        PpmImage image;
        image.LoadImage(filename);

        cout << "Which filter? ";
        cin >> filter;

        switch (filter)
        {
        case 1:
            image.ApplyFilter1();
            break;

        case 2:
            image.ApplyFilter2();
            break;
        }

        cout << "Output filename: ";
        cin.ignore();
        getline(cin, filename);
        image.SaveImage(filename);
    }

    return 0;
}
