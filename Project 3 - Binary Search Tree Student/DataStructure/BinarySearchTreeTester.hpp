#ifndef _BINARY_SEARCH_TREE_TESTER_HPP
#define _BINARY_SEARCH_TREE_TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "../cuTEST/TesterBase.hpp"
#include "../Utilities/Menu.hpp"
#include "../Utilities/StringUtil.hpp"
#include "../Utilities/Logger.hpp"
#include "../Exceptions/NotImplementedException.hpp"

#include "BinarySearchTreeNode.hpp"
#include "BinarySearchTree.hpp"

class BinarySearchTreeTester : public TesterBase
{
public:
    BinarySearchTreeTester()
    {
        AddTest(TestListItem("NodeConsructor()",    bind(&BinarySearchTreeTester::Test_NodeConstructor, this)));

        AddTest(TestListItem("TreeConstructor()",   bind(&BinarySearchTreeTester::Test_TreeConstructor, this)));

        AddTest(TestListItem("Push()",              bind(&BinarySearchTreeTester::Test_Push, this)));
        AddTest(TestListItem("Contains()",          bind(&BinarySearchTreeTester::Test_Contains, this)));
        AddTest(TestListItem("FindNode()",          bind(&BinarySearchTreeTester::Test_FindNode, this)));
//        AddTest(TestListItem("FindParentOfNode()",  bind(&BinarySearchTreeTester::Test_FindParentOfNode, this)));
        AddTest(TestListItem("GetInOrder()",        bind(&BinarySearchTreeTester::Test_GetInOrder, this)));
        AddTest(TestListItem("GetPreOrder()",       bind(&BinarySearchTreeTester::Test_GetPreOrder, this)));
        AddTest(TestListItem("GetPostOrder()",      bind(&BinarySearchTreeTester::Test_GetPostOrder, this)));
        AddTest(TestListItem("GetMinKey()",         bind(&BinarySearchTreeTester::Test_GetMinKey, this)));
        AddTest(TestListItem("GetMaxKey()",         bind(&BinarySearchTreeTester::Test_GetMaxKey, this)));
        AddTest(TestListItem("GetCount()",          bind(&BinarySearchTreeTester::Test_GetCount, this)));
        AddTest(TestListItem("GetHeight()",         bind(&BinarySearchTreeTester::Test_GetHeight, this)));
//        AddTest(TestListItem("Delete()",            bind(&BinarySearchTreeTester::Test_Delete, this)));
    }

    virtual ~BinarySearchTreeTester() { }

private:
    int Test_NodeConstructor();
    int Test_TreeConstructor();
    int Test_Push();
    int Test_Contains();
    int Test_FindNode();
//    int Test_FindParentOfNode();
    int Test_GetInOrder();
    int Test_GetPreOrder();
    int Test_GetPostOrder();
    int Test_GetMinKey();
    int Test_GetMaxKey();
    int Test_GetCount();
    int Test_GetHeight();
//    int Test_Delete();
};

int BinarySearchTreeTester::Test_NodeConstructor()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_NodeConstructor", 3 );
    StartTestSet( "Test_NodeConstructor", { } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "1. Check if Node Constructor has been implemented yet..." );

        bool prereqsImplemented = true;
        string functionName = "Node Constructor";
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            Node<char, string> testNode;
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    StartTest( "2. Create new node, ptrLeft/ptrRight should be nullptr." );
    {
        Set_Comments( "Make sure you're initializing pointers to nullptr from within constructors!" );

        Node<char, string> node;

        Set_ExpectedOutput  ( "Node's ptrLeft is nullptr" );
        Set_ExpectedOutput  ( "Node's ptrRight is nullptr" );

        oss << node.ptrLeft;
        Set_ActualOutput    ( "Node's ptrLeft", oss.str() );
        oss.clear();
        oss << node.ptrRight;
        Set_ActualOutput    ( "Node's ptrRight", oss.str() );

        if ( node.ptrLeft != nullptr )
        {
            TestFail();
        }
        else if ( node.ptrRight != nullptr )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }
    }
    FinishTest();

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_TreeConstructor()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_TreeConstructor", 3 );
    StartTestSet( "Test_TreeConstructor", { } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "1. Create new tree, check that m_ptrRoot is nullptr and m_nodeCount is 0." );

        Set_Comments( "Make sure you're initializing pointers to nullptr from within constructors!" );

        BinarySearchTree<char, string> tree;

        int expectedResult = 0;
        int actualResult = tree.m_nodeCount;

        Set_ExpectedOutput  ( "m_nodeCount", expectedResult );
        Set_ActualOutput    ( "m_nodeCount", actualResult );

        oss << tree.m_ptrRoot;
        Set_ExpectedOutput  ( "Trees's m_ptrRoot", string( "nullptr" ) );
        Set_ActualOutput    ( "Trees's m_ptrRoot", oss.str() );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else if ( tree.m_ptrRoot != nullptr )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_Push()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_Push", 3 );
    StartTestSet( "Test_Push", { "RecursivePush", "Contains" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::RecursivePush";
        StartTest( "1a. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.Push('z', "Z");
            tree.RecursivePush( 'a', "A", tree.m_ptrRoot );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::Push";
        StartTest( "1b. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.Push( 'a', "A" );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "2. Push one item to a tree. Make sure it becomes the root. Validate key/data." );

        Set_Comments( "Push( 'a', \"apple\" )" );

        BinarySearchTree<char, string> tree;
        tree.Push( 'a', "apple" );

        oss << tree.m_ptrRoot;
        Set_ExpectedOutput  ( "Trees's m_ptrRoot", string( "NOT nullptr" ) );
        Set_ActualOutput    ( "Trees's m_ptrRoot", oss.str() );

        char   expectedKey  = 'a';
        char   actualKey    = '-';
        string expectedData = "apple";
        string actualData   = "-";

        Set_ExpectedOutput  ( "m_ptrRoot's key", expectedKey );
        Set_ExpectedOutput  ( "m_ptrRoot's data", expectedData );

        if ( tree.m_ptrRoot != nullptr )
        {
            actualKey = tree.m_ptrRoot->key;
            actualData = tree.m_ptrRoot->data;
            Set_ActualOutput    ( "m_ptrRoot's key", actualKey );
            Set_ActualOutput    ( "m_ptrRoot's data", actualData );
        }

        if ( tree.m_ptrRoot == nullptr )
        {
            Set_Comments( "m_ptrRoot was nullptr" );
            TestFail();
        }
        else if ( actualKey != expectedKey )
        {
            Set_Comments( "keys didn't match" );
            TestFail();
        }
        else if ( actualData != expectedData )
        {
            Set_Comments( "data didn't match" );
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "3. Push one item to a tree. Make sure it becomes the root." );

        Set_Comments( "Push( 'a', \"apple\" )" );

        BinarySearchTree<char, string> tree;
        tree.Push( 'a', "apple" );

        oss << tree.m_ptrRoot;
        Set_ExpectedOutput  ( "Trees's m_ptrRoot", string( "NOT nullptr" ) );
        Set_ActualOutput    ( "Trees's m_ptrRoot", oss.str() );

        if ( tree.m_ptrRoot == nullptr )
        {
            Set_Comments( "m_ptrRoot was nullptr" );
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "4. Push items to a tree. Check positions." );

        Set_Comments( "Push( 'b', \"banana\" )" );
        Set_Comments( "Push( 'a', \"apple\" )" );
        Set_Comments( "Push( 'c', \"cranberry\" )" );

        BinarySearchTree<char, string> tree;
        tree.Push( 'b', "banana" );
        tree.Push( 'a', "apple" );
        tree.Push( 'c', "cranberry" );


        string txtTree = "<pre>";
        txtTree += "  b     \n";
        txtTree += " / \\   \n";
        txtTree += "a   c   \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );

        Set_ExpectedOutput  ( "m_ptrRoot->key", 'b' );
        Set_ExpectedOutput  ( "m_ptrRoot->data", string( "banana" ) );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrLeft->key", 'a' );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrLeft->data", string( "apple" ) );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrRight->key", 'c' );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrRight->data", string( "cranberry" ) );

        if ( tree.m_ptrRoot != nullptr )
        {
            Set_ActualOutput    ( "m_ptrRoot->key", tree.m_ptrRoot->key );
            Set_ActualOutput    ( "m_ptrRoot->data", tree.m_ptrRoot->data );

            if ( tree.m_ptrRoot->ptrLeft != nullptr )
            {
                Set_ActualOutput    ( "m_ptrRoot->ptrLeft->key", tree.m_ptrRoot->ptrLeft->key );
                Set_ActualOutput    ( "m_ptrRoot->ptrLeft->data", tree.m_ptrRoot->ptrLeft->data );
            }
            if ( tree.m_ptrRoot->ptrRight != nullptr )
            {
                Set_ActualOutput    ( "m_ptrRoot->ptrRight->key", tree.m_ptrRoot->ptrRight->key );
                Set_ActualOutput    ( "m_ptrRoot->ptrRight->data", tree.m_ptrRoot->ptrRight->data );
            }
        }

        if ( tree.m_ptrRoot == nullptr )
        {
            Set_ActualOutput( "m_ptrRoot", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->key != 'b' )
        {
            Set_Comments( "m_ptrRoot key was not 'b'" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrLeft == nullptr )
        {
            Set_ActualOutput( "m_ptrRoot->ptrLeft", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot->ptrLeft was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrLeft->key != 'a' )
        {
            Set_Comments( "m_ptrRoot->ptrLeft key was not 'a'" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrRight == nullptr )
        {
            Set_ActualOutput( "m_ptrRoot->ptrRight", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot->ptrRight was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrRight->key != 'c' )
        {
            Set_Comments( "m_ptrRoot->ptrRight key was not 'c'" );
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "5. Push items to a tree. Check positions." );

        Set_Comments( "Push( 'd', \"dates\" )" );
        Set_Comments( "Push( 'b', \"banana\" )" );
        Set_Comments( "Push( 'f', \"figs\" )" );
        Set_Comments( "Push( 'a', \"apple\" )" );
        Set_Comments( "Push( 'c', \"cranberry\" )" );
        Set_Comments( "Push( 'e', \"elderberry\" )" );
        Set_Comments( "Push( 'g', \"grapefruit\" )" );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         f           \n";
        txtTree += " / \\       / \\        \n";
        txtTree += "a   c     e   g         \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );

        BinarySearchTree<char, string> tree;
        tree.Push( 'd', "dates" );
        tree.Push( 'b', "banana" );
        tree.Push( 'f', "figs" );
        tree.Push( 'a', "apple" );
        tree.Push( 'c', "cranberry" );
        tree.Push( 'e', "elderberry" );
        tree.Push( 'g', "grapefruit" );

        Set_ExpectedOutput  ( "m_ptrRoot->key", 'd' );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrLeft->key", 'b' );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrLeft->ptrLeft->key",  'a' );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrLeft->ptrRight->key", 'c' );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrRight->key", 'f' );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrRight->ptrLeft->key",  'e' );
        Set_ExpectedOutput  ( "m_ptrRoot->ptrRight->ptrRight->key", 'g' );

        if ( tree.m_ptrRoot != nullptr )
        {
            Set_ActualOutput    ( "m_ptrRoot->key", tree.m_ptrRoot->key );

            if ( tree.m_ptrRoot->ptrLeft != nullptr )
            {
                Set_ActualOutput    ( "m_ptrRoot->ptrLeft->key", tree.m_ptrRoot->ptrLeft->key );

                if ( tree.m_ptrRoot->ptrLeft->ptrLeft != nullptr )
                {
                    Set_ActualOutput    ( "m_ptrRoot->ptrLeft->ptrLeft->key", tree.m_ptrRoot->ptrLeft->ptrLeft->key );
                }
                if ( tree.m_ptrRoot->ptrLeft->ptrRight != nullptr )
                {
                    Set_ActualOutput    ( "m_ptrRoot->ptrLeft->ptrRight->key", tree.m_ptrRoot->ptrLeft->ptrRight->key );
                }
            }
            if ( tree.m_ptrRoot->ptrRight != nullptr )
            {
                Set_ActualOutput    ( "m_ptrRoot->ptrRight->key", tree.m_ptrRoot->ptrRight->key );

                if ( tree.m_ptrRoot->ptrRight->ptrLeft != nullptr )
                {
                    Set_ActualOutput    ( "m_ptrRoot->ptrRight->ptrLeft->key", tree.m_ptrRoot->ptrRight->ptrLeft->key );
                }
                if ( tree.m_ptrRoot->ptrRight->ptrRight != nullptr )
                {
                    Set_ActualOutput    ( "m_ptrRoot->ptrRight->ptrRight->key", tree.m_ptrRoot->ptrRight->ptrRight->key );
                }
            }
        }

        /*
               d
            /     \
          b         f
         / \       / \
        a   c     e   g
        */

        if ( tree.m_ptrRoot == nullptr )
        {
            Set_ActualOutput( "m_ptrRoot", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->key != 'd' )
        {
            /*
                  [d]
                /     \
              b         f
             / \       / \
            a   c     e   g
            */
            Set_Comments( "m_ptrRoot key was not 'd'" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrLeft == nullptr )
        {
            /*
                   d
                /     \
              [b]      f
             / \       / \
            a   c     e   g
            */
            Set_ActualOutput( "m_ptrRoot->ptrLeft", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot->ptrLeft was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrLeft->key != 'b' )
        {
            Set_Comments( "m_ptrRoot->ptrLeft key was not 'b'" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrLeft->ptrLeft == nullptr )
        {
            /*
                   d
                /     \
              b         f
             / \       / \
            [a]  c     e   g
            */
            Set_ActualOutput( "m_ptrRoot->ptrLeft->ptrLeft", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot->ptrLeft->ptrLeft was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrLeft->ptrLeft->key != 'a' )
        {
            Set_Comments( "m_ptrRoot->ptrLeft->ptrLeft->key was not 'a'" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrLeft->ptrRight == nullptr )
        {
            /*
                   d
                /     \
              b         f
             / \       / \
            a  [c]     e   g
            */
            Set_ActualOutput( "m_ptrRoot->ptrLeft->ptrRight", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot->ptrLeft->ptrRight was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrLeft->ptrRight->key != 'c' )
        {
            Set_Comments( "m_ptrRoot->ptrLeft->ptrRight->key was not 'c'" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrRight == nullptr )
        {
            /*
                   d
                /     \
              b       [f]
             / \       / \
            a   c     e   g
            */
            Set_ActualOutput( "m_ptrRoot->ptrRight", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot->ptrRight was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrRight->key != 'f' )
        {
            Set_Comments( "m_ptrRoot->ptrRight key was not 'f'" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrRight->ptrLeft == nullptr )
        {
            /*
                   d
                /     \
              b         f
             / \       / \
            a  c     [e]  g
            */
            Set_ActualOutput( "m_ptrRoot->ptrRight->ptrLeft", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot->ptrRight->ptrLeft was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrRight->ptrLeft->key != 'e' )
        {
            Set_Comments( "m_ptrRoot->ptrLeft->ptrLeft->key was not 'e'" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrRight->ptrRight == nullptr )
        {
            /*
                   d
                /     \
              b         f
             / \       / \
            a   c     e  [g]
            */
            Set_ActualOutput( "m_ptrRoot->ptrRight->ptrRight", string( "nullptr" ) );
            Set_Comments( "m_ptrRoot->ptrRight->ptrRight was nullptr" );
            TestFail();
        }
        else if ( tree.m_ptrRoot->ptrRight->ptrRight->key != 'g' )
        {
            Set_Comments( "m_ptrRoot->ptrRight->ptrRight->key was not 'g'" );
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/


    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::Contains";
        StartTest( "6. Check if prerequisite function " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.Contains( 'a' );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "7. Don't allow the same key to be entered more than once. Throw runtime_error if this happens." );

        Set_Comments( "Push( 'a', \"apple\" )" );
        Set_Comments( "Push( 'a', \"apple\" )" );

        BinarySearchTree<char, string> tree;

        tree.Push( 'a', "apple" );

        bool exceptionHappened = false;
        try
        {
            tree.Push( 'a', "aardvark" );
        }
        catch( runtime_error ex )
        {
            exceptionHappened = true;
        }

        Set_ExpectedOutput  ( "Exception thrown", bool( true ) );
        Set_ActualOutput    ( "Exception thrown", bool( exceptionHappened ) );

        if ( !exceptionHappened )
        {
            Set_Comments( "Expected a runtime_error to be thrown but it didn't happen!" );
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/



    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_Contains()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_Contains", 3 );
    StartTestSet( "Test_Contains", { "RecursiveContains" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::RecursiveContains";
        StartTest( "1a. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.RecursiveContains( 'a', tree.m_ptrRoot );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::Contains";
        StartTest( "1b. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.Contains( 'a' );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "2. Add some items to a tree; Check if Contains() locates an item." );

        Set_Comments( "Push( 'b', \"banana\" )" );
        Set_Comments( "Push( 'a', \"apple\" )" );
        Set_Comments( "Push( 'c', \"cranberry\" )" );

        string txtTree = "<pre>";
        txtTree += "  b     \n";
        txtTree += " / \\   \n";
        txtTree += "a   c   \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'a', "apple" );
        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'c', "cantalope" );

        bool expectedResult = true;
        bool actualResult = tree.Contains( 'c' );

        Set_ExpectedOutput  ( "tree.Contains( 'c' )", bool(expectedResult) );
        Set_ActualOutput    ( "tree.Contains( 'c' )", bool(actualResult) );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "3. Add some items to a tree; Check if Contains() returns false for item not in tree." );

        Set_Comments( "Push( 'b', \"banana\" )" );
        Set_Comments( "Push( 'a', \"apple\" )" );
        Set_Comments( "Push( 'c', \"cranberry\" )" );

        string txtTree = "<pre>";
        txtTree += "  b     \n";
        txtTree += " / \\   \n";
        txtTree += "a   c   \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'a', "apple" );
        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'c', "cantalope" );

        bool expectedResult = false;
        bool actualResult = tree.Contains( 'z' );

        Set_ExpectedOutput  ( "tree.Contains( 'z' )", bool(expectedResult) );
        Set_ActualOutput    ( "tree.Contains( 'z' )", bool(actualResult) );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_FindNode()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_FindNode", 3 );
    StartTestSet( "Test_FindNode", { "RecursiveFindNode" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::RecursiveFindNode";
        StartTest( "1a. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.RecursiveFindNode( 'a', tree.m_ptrRoot );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::FindNode";
        StartTest( "1b. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.FindNode( 'a' );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Fill a tree, use FindNode() to find node 'g' in the tree." );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         f           \n";
        txtTree += " / \\       / \\        \n";
        txtTree += "a   c     e   g         \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );
        Set_Comments( "FindNode( 'g' )" );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'd', "dates" );

        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrLeft->ptrLeft = new Node<char,string>( 'a', "apple" );
        tree.m_ptrRoot->ptrLeft->ptrRight = new Node<char,string>( 'c', "carrot" );

        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'f', "figs" );
        tree.m_ptrRoot->ptrRight->ptrLeft = new Node<char,string>( 'e', "eggplant" );
        tree.m_ptrRoot->ptrRight->ptrRight = new Node<char,string>( 'g', "grapefruit" );

        char expectedKey = 'g';
        string expectedData = "grapefruit";

        Set_ExpectedOutput  ( "found node address",  string("NOT nullptr") );
        Set_ExpectedOutput  ( "found node key",  char(expectedKey) );
        Set_ExpectedOutput  ( "found node data", string(expectedData) );

        char actualKey = '-';
        string actualData = "-";

        Node<char, string>* findNode = tree.FindNode( 'g' );

        if ( findNode == nullptr )
        {
            Set_ActualOutput  ( "found node address",  string("nullptr") );
        }
        else
        {
            actualKey = findNode->key;
            actualData = findNode->data;

            Set_ActualOutput    ( "found node key", actualKey );
            Set_ActualOutput    ( "found node data", actualData );
        }

        if ( findNode == nullptr )
        {
            TestFail();
            Set_Comments( "FindNode returned nullptr!" );
        }
        else if ( actualKey != expectedKey )
        {
            TestFail();
            Set_Comments( "FindNode key was wrong!" );
        }
        else if ( actualData != expectedData )
        {
            TestFail();
            Set_Comments( "FindNode data was wrong!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Fill a tree, use FindNode() for item not in tree ('z'). Result should be nullptr." );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         f           \n";
        txtTree += " / \\       / \\        \n";
        txtTree += "a   c     e   g         \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );
        Set_Comments( "FindNode( 'z' )" );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'd', "dates" );

        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrLeft->ptrLeft = new Node<char,string>( 'a', "apple" );
        tree.m_ptrRoot->ptrLeft->ptrRight = new Node<char,string>( 'c', "carrot" );

        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'f', "figs" );
        tree.m_ptrRoot->ptrRight->ptrLeft = new Node<char,string>( 'e', "eggplant" );
        tree.m_ptrRoot->ptrRight->ptrRight = new Node<char,string>( 'g', "grapefruit" );

        Set_ExpectedOutput  ( "found node address",  string("nullptr") );

        Node<char, string>* findNode = tree.FindNode( 'z' );

        if ( findNode == nullptr )
        {
            Set_ActualOutput  ( "found node address",  string("nullptr") );
        }
        else
        {
            oss << findNode;
            Set_ActualOutput    ( "found node address", oss.str() );
        }

        if ( findNode != nullptr )
        {
            TestFail();
            Set_Comments( "FindNode was supposd to return nullptr!" );
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_GetInOrder()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_GetInOrder", 3 );
    StartTestSet( "Test_GetInOrder", { "RecursiveGetInOrder" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::RecursiveGetInOrder";
        StartTest( "1a. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            stringstream stream;
            tree.RecursiveGetInOrder( tree.m_ptrRoot, stream );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::GetInOrder";
        StartTest( "1b. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.GetInOrder();
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Add things to a tree and then check the InOrder traversal." );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         f           \n";
        txtTree += " / \\       / \\        \n";
        txtTree += "a   c     e   g         \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );
        Set_Comments( "InOrder traversal: LEFT, SELF, RIGHT" );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'd', "dates" );
        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrLeft->ptrLeft = new Node<char,string>( 'a', "apple" );
        tree.m_ptrRoot->ptrLeft->ptrRight = new Node<char,string>( 'c', "carrot" );
        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'f', "figs" );
        tree.m_ptrRoot->ptrRight->ptrLeft = new Node<char,string>( 'e', "eggplant" );
        tree.m_ptrRoot->ptrRight->ptrRight = new Node<char,string>( 'g', "grapefruit" );

        string expectedResult = "abcdefg";
        string actualResult = tree.GetInOrder();

        Set_ExpectedOutput  ( "GetInOrder()", expectedResult );
        Set_ActualOutput    ( "GetInOrder()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_GetPreOrder()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_GetPreOrder", 3 );
    StartTestSet( "Test_GetPreOrder", { "RecursiveGetPreOrder" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::RecursiveGetPreOrder";
        StartTest( "1a. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            stringstream stream;
            tree.RecursiveGetPreOrder( tree.m_ptrRoot, stream );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::GetPreOrder";
        StartTest( "1b. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.GetPreOrder();
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Add things to a tree and then check the PreOrder traversal." );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         f           \n";
        txtTree += " / \\       / \\        \n";
        txtTree += "a   c     e   g         \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );
        Set_Comments( "PreOrder traversal: SELF, LEFT, RIGHT" );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'd', "dates" );
        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrLeft->ptrLeft = new Node<char,string>( 'a', "apple" );
        tree.m_ptrRoot->ptrLeft->ptrRight = new Node<char,string>( 'c', "carrot" );
        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'f', "figs" );
        tree.m_ptrRoot->ptrRight->ptrLeft = new Node<char,string>( 'e', "eggplant" );
        tree.m_ptrRoot->ptrRight->ptrRight = new Node<char,string>( 'g', "grapefruit" );

        string expectedResult = "dbacfeg";
        string actualResult = tree.GetPreOrder();

        Set_ExpectedOutput  ( "GetPreOrder()", expectedResult );
        Set_ActualOutput    ( "GetPreOrder()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_GetPostOrder()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_GetPostOrder", 3 );
    StartTestSet( "Test_GetPostOrder", { "RecursiveGetPostOrder" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::RecursiveGetPostOrder";
        StartTest( "1a. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            stringstream stream;
            tree.RecursiveGetPostOrder( tree.m_ptrRoot, stream );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::GetPostOrder";
        StartTest( "1b. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.GetPostOrder();
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Add things to a tree and then check the PostOrder traversal." );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         f           \n";
        txtTree += " / \\       / \\        \n";
        txtTree += "a   c     e   g         \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );
        Set_Comments( "PostOrder traversal: LEFT, RIGHT, SELF" );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'd', "dates" );
        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrLeft->ptrLeft = new Node<char,string>( 'a', "apple" );
        tree.m_ptrRoot->ptrLeft->ptrRight = new Node<char,string>( 'c', "carrot" );
        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'f', "figs" );
        tree.m_ptrRoot->ptrRight->ptrLeft = new Node<char,string>( 'e', "eggplant" );
        tree.m_ptrRoot->ptrRight->ptrRight = new Node<char,string>( 'g', "grapefruit" );

        string expectedResult = "acbegfd";
        string actualResult = tree.GetPostOrder();

        Set_ExpectedOutput  ( "GetPostOrder()", expectedResult );
        Set_ActualOutput    ( "GetPostOrder()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_GetMaxKey()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_GetMaxKey", 3 );
    StartTestSet( "Test_GetMaxKey", { "RecursiveGetMax" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::RecursiveGetMax";
        StartTest( "1a. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.RecursiveGetMax( tree.m_ptrRoot );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }
        catch( ... )
        {
            // Anything else is fine
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::GetMaxKey";
        StartTest( "1b. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.GetMaxKey();
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }
        catch( ... )
        {
            // Anything else is fine
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create a tree and then get the max key." );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         f           \n";
        txtTree += " / \\       / \\        \n";
        txtTree += "a   c     e   g         \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );
        Set_Comments( "Max key: Keep traversing RIGHT until there are no more nodes." );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'd', "dates" );
        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrLeft->ptrLeft = new Node<char,string>( 'a', "apple" );
        tree.m_ptrRoot->ptrLeft->ptrRight = new Node<char,string>( 'c', "carrot" );
        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'f', "figs" );
        tree.m_ptrRoot->ptrRight->ptrLeft = new Node<char,string>( 'e', "eggplant" );
        tree.m_ptrRoot->ptrRight->ptrRight = new Node<char,string>( 'g', "grapefruit" );

        char expectedResult = 'g';
        char actualResult = tree.GetMaxKey();

        Set_ExpectedOutput  ( "GetMaxKey()", expectedResult );
        Set_ActualOutput    ( "GetMaxKey()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_GetMinKey()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_GetMinKey", 3 );
    StartTestSet( "Test_GetMinKey", { "RecursiveGetMin" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::RecursiveGetMin";
        StartTest( "1a. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.RecursiveGetMin( tree.m_ptrRoot );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }
        catch( ... )
        {
            // Anything else is fine
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::GetMinKey";
        StartTest( "1b. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.GetMinKey();
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }
        catch( ... )
        {
            // Anything else is fine
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create a tree and then get the min key." );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         f           \n";
        txtTree += " / \\       / \\        \n";
        txtTree += "a   c     e   g         \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );
        Set_Comments( "Max key: Keep traversing LEFT until there are no more nodes." );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'd', "dates" );
        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrLeft->ptrLeft = new Node<char,string>( 'a', "apple" );
        tree.m_ptrRoot->ptrLeft->ptrRight = new Node<char,string>( 'c', "carrot" );
        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'f', "figs" );
        tree.m_ptrRoot->ptrRight->ptrLeft = new Node<char,string>( 'e', "eggplant" );
        tree.m_ptrRoot->ptrRight->ptrRight = new Node<char,string>( 'g', "grapefruit" );

        char expectedResult = 'a';
        char actualResult = tree.GetMinKey();

        Set_ExpectedOutput  ( "GetMinKey()", expectedResult );
        Set_ActualOutput    ( "GetMinKey()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_GetCount()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_GetCount", 3 );
    StartTestSet( "Test_GetCount", { "Push" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::Push";
        StartTest( "1a. Check if prerequisite function " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.Push( 'a', "A" );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::GetCount";
        StartTest( "1b. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.GetCount();
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create an empty tree and get the item count." );

        BinarySearchTree<char, string> tree;

        int expectedResult = 0;
        int actualResult = tree.GetCount();

        Set_ExpectedOutput  ( "GetCount()", expectedResult );
        Set_ActualOutput    ( "GetCount()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create a tree and get the item count." );

        Set_Comments( "Push( 'd', \"dates\" )" );
        Set_Comments( "Push( 'b', \"banana\" )" );
        Set_Comments( "Push( 'f', \"figs\" )" );
        Set_Comments( "Push( 'a', \"apple\" )" );
        Set_Comments( "Push( 'c', \"cranberry\" )" );
        Set_Comments( "Push( 'e', \"elderberry\" )" );
        Set_Comments( "Push( 'g', \"grapefruit\" )" );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         f           \n";
        txtTree += " / \\       / \\        \n";
        txtTree += "a   c     e   g         \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );

        BinarySearchTree<char, string> tree;
        tree.Push( 'd', "dates" );
        tree.Push( 'b', "banana" );
        tree.Push( 'f', "figs" );
        tree.Push( 'a', "apple" );
        tree.Push( 'c', "cranberry" );
        tree.Push( 'e', "elderberry" );
        tree.Push( 'g', "grapefruit" );

        int expectedResult = 7;
        int actualResult = tree.GetCount();

        Set_ExpectedOutput  ( "GetCount()", expectedResult );
        Set_ActualOutput    ( "GetCount()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int BinarySearchTreeTester::Test_GetHeight()
{
    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_GetHeight", 3 );
    StartTestSet( "Test_GetHeight", { "RecursiveGetHeight" } );
    ostringstream oss;

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::RecursiveGetHeight";
        StartTest( "1. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.RecursiveGetHeight( tree.m_ptrRoot );
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        string functionName = "BinarySearchTree::GetHeight";
        StartTest( "1c. Check if " + functionName + " has been implemented yet..." );

        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, string( "Implemented" ) );
        try
        {
            BinarySearchTree<char, string> tree;
            tree.GetHeight();
        }
        catch ( NotImplementedException& ex )
        {
            Set_Comments( ex.what() );
            prereqsImplemented = false;
        }

        if ( prereqsImplemented )
        {
            Set_ActualOutput( functionName, string( "Implemented" ) );
            TestPass();
            FinishTest();
        }
        else
        {
            Set_ActualOutput( functionName, string( "Not implemented" ) );
            TestFail();
            FinishTest();
            FinishTestSet();
            return TestResult();
        }
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create tree with one node (root), check height." );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'a', "apple" );

        Set_Comments( "Height of a node is the # of EDGES from the node to the deepest leaf." );

        int expectedResult = 0;
        int actualResult = tree.GetHeight();

        Set_ExpectedOutput  ( "GetHeight()", expectedResult );
        Set_ActualOutput    ( "GetHeight()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    {
        /* TEST BEGIN ************************************************************/
        StartTest( "Create tree with several nodes, check height." );

        BinarySearchTree<char, string> tree;
        tree.m_ptrRoot = new Node<char,string>( 'd', "dates" );
        tree.m_ptrRoot->ptrLeft = new Node<char,string>( 'b', "banana" );
        tree.m_ptrRoot->ptrRight = new Node<char,string>( 'g', "grapefruit" );
        tree.m_ptrRoot->ptrRight->ptrLeft = new Node<char,string>( 'e', "elderberry" );
        tree.m_ptrRoot->ptrRight->ptrLeft->ptrRight = new Node<char,string>( 'f', "elderberry" );

        string txtTree = "<pre>";
        txtTree += "       d                \n";
        txtTree += "    /     \\            \n";
        txtTree += "  b         g           \n";
        txtTree += "           /            \n";
        txtTree += "          e             \n";
        txtTree += "           \            \n";
        txtTree += "            f           \n";
        txtTree += "</pre>";
        Set_Comments( txtTree );
        Set_Comments( "Height of a node is the # of EDGES from the node to the deepest leaf." );

        int expectedResult = 3;
        int actualResult = tree.GetHeight();

        Set_ExpectedOutput  ( "GetHeight()", expectedResult );
        Set_ActualOutput    ( "GetHeight()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}


//int BinarySearchTreeTester::Test_FindParentOfNode()
//{
//    Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_FindParentOfNode", 3 );
//    StartTestSet( "Test_FindParentOfNode", { "RecursiveFindNode", "Push" } );
//    ostringstream oss;
//
//    { /* TEST BEGIN ************************************************************/
//        string functionName = "BinarySearchTree::FindParentOfNode";
//        StartTest( "1. Check if " + functionName + " has been implemented yet..." );
//
//        bool prereqsImplemented = true;
//        Set_ExpectedOutput( functionName, string( "Implemented" ) );
//        try
//        {
//            BinarySearchTree<char, string> tree;
//            tree.FindParentOfNode( 'a' );
//        }
//        catch ( NotImplementedException& ex )
//        {
//            Set_Comments( ex.what() );
//            prereqsImplemented = false;
//        }
//
//        if ( prereqsImplemented )
//        {
//            Set_ActualOutput( functionName, string( "Implemented" ) );
//            TestPass();
//            FinishTest();
//        }
//        else
//        {
//            Set_ActualOutput( functionName, string( "Not implemented" ) );
//            TestFail();
//            FinishTest();
//            FinishTestSet();
//            return TestResult();
//        }
//    } /* TEST END **************************************************************/
//
//    { /* TEST BEGIN ************************************************************/
//        StartTest( "Fill a tree, use FindNode() to find node in the tree." );
//
//        Set_Comments( "Push( 'd', \"dates\" )" );
//        Set_Comments( "Push( 'b', \"banana\" )" );
//        Set_Comments( "Push( 'f', \"figs\" )" );
//        Set_Comments( "Push( 'a', \"apple\" )" );
//        Set_Comments( "Push( 'c', \"cranberry\" )" );
//        Set_Comments( "Push( 'e', \"elderberry\" )" );
//        Set_Comments( "Push( 'g', \"grapefruit\" )" );
//
//        string txtTree = "<pre>";
//        txtTree += "       d                \n";
//        txtTree += "    /     \\            \n";
//        txtTree += "  b         f           \n";
//        txtTree += " / \\       / \\        \n";
//        txtTree += "a   c     e   g         \n";
//        txtTree += "</pre>";
//        Set_Comments( txtTree );
//        Set_Comments( "FindNode( 'g' )" );
//
//        BinarySearchTree<char, string> tree;
//        tree.Push( 'd', "dates" );
//        tree.Push( 'b', "banana" );
//        tree.Push( 'f', "figs" );
//        tree.Push( 'a', "apple" );
//        tree.Push( 'c', "cranberry" );
//        tree.Push( 'e', "elderberry" );
//        tree.Push( 'g', "grapefruit" );
//
//        char expectedKey = 'g';
//        string expectedData = "grapefruit";
//
//        Set_ExpectedOutput  ( "found node address",  string("NOT nullptr") );
//        Set_ExpectedOutput  ( "found node key",  char(expectedKey) );
//        Set_ExpectedOutput  ( "found node data", string(expectedData) );
//
//        char actualKey = '-';
//        string actualData = "-";
//
//        Node<char, string>* findNode = tree.FindNode( 'g' );
//
//        if ( findNode == nullptr )
//        {
//            Set_ActualOutput  ( "found node address",  string("nullptr") );
//        }
//        else
//        {
//            actualKey = findNode->key;
//            actualData = findNode->data;
//
//            Set_ActualOutput    ( "found node key", actualKey );
//            Set_ActualOutput    ( "found node data", actualData );
//        }
//
//        if ( findNode == nullptr )
//        {
//            TestFail();
//            Set_Comments( "FindNode returned nullptr!" );
//        }
//        else if ( actualKey != expectedKey )
//        {
//            TestFail();
//            Set_Comments( "FindNode key was wrong!" );
//        }
//        else if ( actualData != expectedData )
//        {
//            TestFail();
//            Set_Comments( "FindNode data was wrong!" );
//        }
//        else
//        {
//            TestPass();
//        }
//
//        FinishTest();
//    } /* TEST END **************************************************************/
//
//    { /* TEST BEGIN ************************************************************/
//        StartTest( "Fill a tree, use FindNode() for item not in tree. Result should be nullptr." );
//
//        Set_Comments( "Push( 'd', \"dates\" )" );
//        Set_Comments( "Push( 'b', \"banana\" )" );
//        Set_Comments( "Push( 'f', \"figs\" )" );
//        Set_Comments( "Push( 'a', \"apple\" )" );
//        Set_Comments( "Push( 'c', \"cranberry\" )" );
//        Set_Comments( "Push( 'e', \"elderberry\" )" );
//        Set_Comments( "Push( 'g', \"grapefruit\" )" );
//
//        string txtTree = "<pre>";
//        txtTree += "       d                \n";
//        txtTree += "    /     \\            \n";
//        txtTree += "  b         f           \n";
//        txtTree += " / \\       / \\        \n";
//        txtTree += "a   c     e   g         \n";
//        txtTree += "</pre>";
//        Set_Comments( txtTree );
//        Set_Comments( "FindNode( 'z' )" );
//
//        BinarySearchTree<char, string> tree;
//        tree.Push( 'd', "dates" );
//        tree.Push( 'b', "banana" );
//        tree.Push( 'f', "figs" );
//        tree.Push( 'a', "apple" );
//        tree.Push( 'c', "cranberry" );
//        tree.Push( 'e', "elderberry" );
//        tree.Push( 'g', "grapefruit" );
//
//        Set_ExpectedOutput  ( "found node address",  string("nullptr") );
//
//        Node<char, string>* findNode = tree.FindNode( 'z' );
//
//        if ( findNode == nullptr )
//        {
//            Set_ActualOutput  ( "found node address",  string("nullptr") );
//        }
//        else
//        {
//            oss << findNode;
//            Set_ActualOutput    ( "found node address", oss.str() );
//        }
//
//        if ( findNode != nullptr )
//        {
//            TestFail();
//            Set_Comments( "FindNode was supposd to return nullptr!" );
//        }
//        else
//        {
//            TestPass();
//        }
//
//        FinishTest();
//    } /* TEST END **************************************************************/
//
//    FinishTestSet();
//    return TestResult();
//}



//int BinarySearchTreeTester::Test_Delete()
//{
//    StartTestSet( "Test_Delete", { } );
//    ostringstream oss;
//
//    { /* TEST BEGIN ************************************************************/
//        StartTest( "DESCRIPTION" );
//
//        int expectedResult = 0;
//        int actualResult = 0;
//
//        Set_ExpectedOutput  ( "ITEM", expectedResult );
//        Set_ActualOutput    ( "ITEM", actualResult );
//
//        if ( actualResult != expectedResult )
//        {
//            TestFail();
//        }
//        else
//        {
//            TestPass();
//        }
//
//        FinishTest();
//    } /* TEST END **************************************************************/
//
//    FinishTestSet();
//    return TestResult();
//}

#endif
