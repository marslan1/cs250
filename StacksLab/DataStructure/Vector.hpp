#ifndef _VECTOR_HPP
#define _VECTOR_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

template <typename T>
class Vector
{
private:
    T* m_data;
    int m_itemCount;
    int m_arraySize;

public:
    /* Member Functions */
    // Constructor and Destructor
    Vector();
    ~Vector();

    // Add items
    void PushFront( const T& newItem );
    void PushBack( const T& newItem );

    // Remove items
    void PopFront();
    void PopBack();

    // Return items
    T& GetFront() const;
    T& GetBack() const;

    // Helper functions
    int Size() const;
    bool IsFull() const;
    bool IsEmpty() const;

private:
    // Internal helper functions
//    bool IsInvalidIndex( int index ) const;
    void ShiftLeft( int index );
    void ShiftRight( int index );

    // Memory mangement
    void AllocateMemory( int newSize = 10 );
    void DeallocateMemory();
    void Resize();

    // Special function, since we haven't covered exceptions yet
    void Panic( string message ) const;
    void NotImplemented() const;

    friend class VectorTester;
};

template <typename T>
Vector<T>::Vector() /*                                                             Vector */
{
    m_data = nullptr;
    m_itemCount = 0;
    m_arraySize = 0;
}

template <typename T>
Vector<T>::~Vector() /*                                                            ~Vector */
{
    DeallocateMemory();
}

template <typename T>
bool Vector<T>::IsEmpty() const /*                                                 IsEmpty */
{
    return ( m_itemCount == 0 );
}

template <typename T>
bool Vector<T>::IsFull() const /*                                                  IsFull */
{
    return ( m_itemCount == m_arraySize );
}

template <typename T>
int Vector<T>::Size() const /*                                                     Size */
{
    return m_itemCount;
}

template <typename T>
void Vector<T>::AllocateMemory( int newSize /* = 10 */ ) /*                        AllocateMemory */
{
    if ( m_data == nullptr )
    {
        m_data = new T[newSize];
        m_arraySize = newSize;
        m_itemCount = 0;
    }
    else
    {
        throw logic_error( "Cannot allocate memory - pointer is already pointing to an address!" );
    }
}

template <typename T>
void Vector<T>::DeallocateMemory() /*                                              DeallocateMemory */
{
    if ( m_data != nullptr )
    {
        delete [] m_data;
        m_data = nullptr;
        m_itemCount = 0;
        m_arraySize = 0;
    }
}

template <typename T>
void Vector<T>::Resize() /*                                                        Resize */
{
    int newSize = m_arraySize + 10;

    // Allocate more memory
    T * newArr = new T[ newSize ];

    // Copy over data
    for ( int i = 0; i < m_arraySize; i++ )
    {
        newArr[i] = m_data[i];
    }

    // Free the old array
    delete [] m_data;

    // Update m_data to point to bigger array
    m_data = newArr;

    // m_arraySize update
    m_arraySize = newSize;
}


template <typename T>
void Vector<T>::PushFront( const T& newItem ) /*                                   PushFront */
{
    if ( m_data == nullptr )
    {
        AllocateMemory();
    }
    else if ( IsFull() )
    {
        Resize();
    }

    ShiftRight( 0 );

    m_data[ 0 ] = newItem;
    m_itemCount++;
}

template <typename T>
void Vector<T>::PushBack( const T& newItem ) /*                                   Push */
{
    if ( m_data == nullptr )
    {
        AllocateMemory();
    }
    else if ( IsFull() )
    {
        Resize();
    }

    m_data[ m_itemCount ] = newItem;
    m_itemCount++;
}

template <typename T>
T& Vector<T>::GetFront() const /*                                        GetFront */
{
    return m_data[ 0 ];
}

template <typename T>
T& Vector<T>::GetBack() const /*                                        GetBack */
{
    return m_data[ m_itemCount-1 ];
}

template <typename T>
void Vector<T>::PopFront() /*                                           PopFront */
{
    if ( m_itemCount > 0 )
    {
        ShiftLeft( 0 );
        m_itemCount--;
    }
}

template <typename T>
void Vector<T>::PopBack() /*                                           PopBack */
{
    if ( m_itemCount > 0 )
    {
        m_itemCount--;
    }
}


template <typename T>
void Vector<T>::ShiftLeft( int index )
{
    for ( int i = index; i < m_itemCount-1; i++ )
    {
        m_data[i] = m_data[i+1];
    }
}

template <typename T>
void Vector<T>::ShiftRight( int index )
{
    for ( int i = m_itemCount; i > index; i-- )
    {
        m_data[i] = m_data[i-1];
    }
}


/* ****************************************************************************/
/* ************************************************* FUNCTION TO THROW ERRORS */
/* ****************************************************************************/

//! Call this function if something terrible goes wrong.
template <typename T>
void Vector<T>::Panic( string message ) const /*                                   Panic */
{
    throw logic_error( message );
}

//! Marks when a function hasn't been implemented yet.
template <typename T>
void Vector<T>::NotImplemented() const /*                                          NotImplemented */
{
    throw runtime_error( "Function not implemented yet!" );
}


#endif
