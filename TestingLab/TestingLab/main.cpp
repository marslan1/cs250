#include <iostream>
#include <string>
#include <fstream>
using namespace std;

#include "Tests.hpp"

int main()
{
    ofstream output("test-result.txt");

    // Run tests
    Test_GetPerimeter(output);
    Test_GetFirstLetter(output);
    Test_Withdraw(output);
    Test_GetArea(output);
    Test_CombineText(output);
    Test_AbsoluteValue(output);

    cout << endl << "Test results in test-result.txt" << endl;

    return 0;
}
