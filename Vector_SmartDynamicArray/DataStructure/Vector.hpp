#ifndef _VECTOR_HPP
#define _VECTOR_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

class Vector
{
    private:
    /* Member Variables */
    //! A string pointer used to allocate memory for a dynamic array.
    string* m_data;

    //! Stores how many items have been added to the Vector; less than or equal to the m_arraySize.
    int m_itemCount;

    //! Stores how large the array currently is; update after Resize() is called.
    int m_arraySize;

    public:
    /* Member Functions */
    // Constructor and Destructor
    Vector()
    {
        m_data = nullptr;
        m_itemCount = 0;
        m_arraySize = 0;

    }
    ~Vector()
    {
        DeallocateMemory();
    }

    // Deal with end of vector
    void Push(const string& newItem)
    {
        /*
        If the m_data pointer is currently nullptr, call the AllocateMemory() function
        before continuing.
        Otherwise, if the array is full (use IsFull()), call the Resize() function 
        before continuing.
        After preparing the array (steps 1 and 2), search for an available space 
        in the array using a for loop. Once you find an empty spot (m_data[i] == ""), 
        then this is the index where you can add the new item.
        Add the new item to the array, and increment the m_itemCount.
        */
        if (m_data == nullptr)
        {
            AllocateMemory();
        }
        else if (IsFull())
        {
            Resize();
        }
        
        for (int i = 0; i < m_arraySize; i++)
        {
            if (m_data[i] == "")
            {
                m_data[i] = newItem;
                m_itemCount++;
                break;
            }

        }

    }

    // Deal with middle of vector
    string Get(int index) const
    {
        if (IsInvalidIndex(index))
        {
            Panic("Invalid index");
            
        }
        else
            return m_data[index];
    }
    void Remove(int index)
    {
        
        if (IsInvalidIndex(index))
        {
            Panic("Invalid index");

        }

        else if (m_data[index] != "")
        {
            m_data[index] = "";
            m_itemCount--;
        }
        else
            throw logic_error("Empty space");
        
    }

    // Helper functions
    int Size() const
    {
        return m_itemCount;
    }
    bool IsFull() const
    {
        return m_itemCount == m_arraySize;
    }
    bool IsEmpty() const
    {
        return m_itemCount == 0;
    }

    private:
    // Internal helper functions
    bool IsInvalidIndex(int index) const
    {
        return (index < 0 || index >= m_arraySize);
    }
    bool IsElementEmpty(int index)
    {
        if (IsInvalidIndex(index))
        {
            Panic("IsElementEmpty index out of bound ");
            
        }
        else
        {
            return m_data[index] == "";
        }
        
    }


    // Memory mangement
    void AllocateMemory(int newSize = 10)
    {
       /* If m_data is currently nullptr, then we can allocate memory
        (otherwise exit the function without doing anything)... Set the 
            m_arraySize to the newSize passed in.Set the m_itemCount to 0 Allocate 
            an array of size m_arraySize using the m_data pointer.*/
        if (m_data == nullptr)
        {
            m_arraySize = newSize;
            m_itemCount = 0;
            m_data = new string[m_arraySize];
        }
        
    }
    void DeallocateMemory()
    {
        /*
        If m_data is already nullptr, nothing needs to be done.
        If m_data is NOT nullptr, then... deallocate memory stored at the m_data location.
        Set m_data to nullptr to prevent invalid memory access.*/
        if (m_data != nullptr)
        {
            delete[] m_data;
            m_data = nullptr;
        }

    }
    void Resize()
    {
        /*
        is called when the array is full. It will allocate a bigger array in memory,
        copy all the data from the old array to the new array, and then update 
        the m_data pointer. Follow these steps:

        Create a new dynamic variable - use a string* pointer, and set its new 
        size to the current array size, plus 10. This is the "big array." 
        (This size is arbitrary; the amount to increase by is a design decision.)
        Use a for loop to copy all items from the old (small) array TO the new big array.
        Afterwards, free up the memory stored at the address pointed to by the m_data
        pointer.
        Update the m_data pointer to point at the same location as the "big array" 
        pointer.
        Update the old array size to the new size (old size + 10).*/

        string* bigArray = new string[m_arraySize + 10];
        for (int i = 0; i < m_arraySize; i++)
        {
            bigArray[i] = m_data[i];

        }
        DeallocateMemory();
        m_data = bigArray;
        m_arraySize += 10;
    }

    // Special function, since we haven't covered exceptions yet
    void Panic(string message) const
    {
        throw runtime_error(message);
    }
    void NotImplemented() const
    {

    }

    friend class Tester;

};

#endif
